<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Lista de Livros</title>
</head>
<body>
    <p>${sucesso}</p>
    <table>
        <tr>
            <td>Título</td>
            <td>Valores</td>
        </tr>
        <c:forEach items="${products}" var="product">
            <tr>
                <td>${product.title}</td>
                <td>
                    <c:forEach items="${product.prices}" var="price">
                        [${price.value} - ${price.bookType}]
                    </c:forEach>
                </td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
